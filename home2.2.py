l=[1,2,3,4,5]

def f(x):
    return x + 5 

#new_list = []
#
#for i in l: 
#   new_list.append(f(i))

for i, v in enumerate(l):
    l[i] = f (v)

#new_list = [f(x) for x in l] 

new_list = list(map(f,l))

print(new_list)